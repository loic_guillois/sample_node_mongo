var express = require('express');
var MongoClient = require('mongodb').MongoClient;

var app = express();

MongoClient.connect(config.mongodb, function(err, db) {
  if(err) {
    console.log("Error : Unable to connect to MongoDB server");
  } else {
    console.log("Connected correctly to MongoDB server");

    app.get('/', function (req, res) {
      db.collection('user').find().toArray(function(err, result) {
        res.send(result);
      })
    });

    var server = app.listen(3000, function () {
      var host = server.address().address;
      var port = server.address().port;

      console.log('Example app listening at http://%s:%s', host, port);
    });
  }
});
