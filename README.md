[Présentation](http://www.lafabriquedecode.com/blog/wp-content/uploads/2013/04/TECH-TALK-001.pdf)

MongoDB 3.0
====================

[Présentation](http://join.adform.com/media/78453/1-henrik-ingo-mongodb.pdf)

* performance +
* stockage utilisé -
* architecture permettant la mise en place de storage engine “plug in” 
* MMAPv1

**WiredTiger**

* anciens de berkeleyDB
* racheté par MongoDB

gère + de core

gère + de mémoire

* concurence (MVCC)
* compression
* snappy
* zlib (+cpu / meilleur compression)

Roadmap :

* LSM (structure de donnée optimisé pour l'écriture)
* Encryption

WiredTiger 64 bits seulement

Prise en main de MongoDB
====================

Lancer le serveur

> mongod

Observer les logs du serveur.

> mongo

Lance un terminal. Obtenir la liste des bases de données :

> show dbs

Utiliser une base particulière :

> use sample

Lister les collections :

> show collections

Insérer quelques données :

* > db.user.insert({name: 'Loic', age: 29, company : { name: 'Conserto' }})
* > db.user.insert({name: 'Joevin', age: 18, company : { name: 'Conserto' }})
* > db.user.insert({name: 'Alban', age: 42, company : { name: 'Conserto' }})

Quelques requêtes :

* > db.user.count()
* > db.user.find()
* > db.user.find({name: 'Loic'})
* > db.user.find({"company.name" : 'conserto'})
* > db.user.find({age : { $gte : 20 }})
* > db.user.remove({name: 'Alban'})
* > db.user.drop()

Aggrégation :

> db.user.aggregate({ $group: { _id: 'conserto', averageAge : { $avg : age}, count : { $sum : 1 } }})

Initialisation du projet Node.js
====================

> npm init

Suivre les instructions.

Création du fichier Node.js
====================

> touch index.js

Editer le contenu :

console.log("hello world");

Installation du client MongoDB
====================

> npm install mongodb --save

Puis ajouter dans le fichier source :

var MongoClient = require('mongodb').MongoClient;

Prise en main des requêtes Mongo dans Node.js

Mise en place de l'API
====================

Installation d'Express.js

> npm install express --save

Open Data de Nantes Métropole
====================

* Choisir votre jeu de données (JSON, CSV, XML)
* Créez un parser
* Proposez une API